import React from 'react';
import {
  Scene,
  Stack,
  Router,
} from 'react-native-router-flux';

// DEMO
import DemoContainer from '../../containers/Demo/Demo';
import DemoComponent from '../components/Demo/Demo';

const Index = (
  <Router hideNavBar>
    <Stack hideNavBar>
      <Scene
        hideNavBar
        initial
        key="lostConnection"
        component={DemoContainer}
        Layout={DemoComponent}
      />
    </Stack>
  </Router>
);

export default Index;
