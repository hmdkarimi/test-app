import { StyleSheet, Dimensions, SCREEN_HEIGHT, Platform } from 'react-native';
import options from '../../vars/variable';
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const btnStyles = StyleSheet.create({
  ///Styles for BUTTONS
  btn: {
    paddingVertical: options.baseOffset,
  },
  outlineBtnSmall: {
    borderWidth: 1,
    borderColor: options.brandPrimary,
    borderRadius: options.borderRadiusBase,
    paddingHorizontal: 15,
    paddingVertical: 3,
    minWidth: 95,
    alignItems: 'center',
  },
  outlineBtnSmallText: {
    color: options.brandPrimary,
    fontSize: 14,
    fontFamily: options.fontFamily,
  },
  roundedBtnSmall: {
    borderWidth: 1,
    borderColor: options.brandPrimary,
    borderRadius: options.borderRadiusBase,
    paddingHorizontal: 15,
    paddingVertical: 3,
    backgroundColor: options.brandPrimary,
    minWidth: 95,
    alignItems: 'center',
  },
  roundedBtnSmallText: {
    color: options.brandWhite,
    fontSize: 14,
    fontFamily: options.fontFamily,
  },
  btnRoundedSmall: {
    backgroundColor: options.brandPrimary,
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 2,
    alignSelf: 'flex-start',
  },
  btnRoundedSmallText: {
    color: options.brandWhite,
    fontSize: 14,
    fontFamily: options.fontFamilySemibold,
  },

  btnRoundedSmallCard: {
    backgroundColor: options.brandPrimary,
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 5,
    // alignSelf:'flex-start'
  },
  btnRoundedSmallTextCard: {
    color: options.brandWhite,
    fontSize: 11,
    fontFamily: options.fontFamilySemibold,
  },
  CircleButton: {
    width: 42,
    height: 42,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 21,
    position: 'relative',
  },
  CircleButtonSmall: {
    width: 36,
    height: 36,
    borderRadius: 18,
  },
  CircleButtonLage: {
    width: 56,
    height: 56,
    borderRadius: 28,
  },
  groupBtn: {
    height: 30,
    width: 115,
  },
});
export default btnStyles;
