import { StyleSheet, Dimensions, SCREEN_HEIGHT, Platform } from 'react-native';
import options from '../../vars/variable';
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const UserViewsStyle = StyleSheet.create({

  //styles for registration view

  avatarImg:{
	borderRadius: options.borderRadiusBox,
	alignSelf:'center',
	overflow:'hidden'

  },
  selectImgBtn:{
	width:160,
	height:160,
	alignSelf:'center',
	borderRadius:options.borderRadiusBox,
	backgroundColor: 'rgba(255,45,85,0.5)',
	flexDirection:'row',
	alignItems:'center',
	justifyContent:'center'

  },
  selectImgBtnPointer:{
	width:14,
	height:14,
	borderRadius:7,
	backgroundColor:'#fff'
  },

});
export default UserViewsStyle;
