import { StyleSheet, Dimensions, SCREEN_HEIGHT, Platform } from 'react-native';
import options from '../../vars/variable';
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const ProfileViewsStyle = StyleSheet.create({

  //styles for User Profile View
  cover:{
	  width:width,
	  height:310,
	  position:'relative'
  },
  overlay:{
	  width:width,
	  height:310,

  }



});
export default ProfileViewsStyle;
