import {
  StyleSheet,
  Dimensions,
  SCREEN_HEIGHT,
  Platform,
} from 'react-native';
import options from '../vars/variable';
import { hidden } from 'ansi-colors';
const IS_IPHONE_X =
  SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT =
  Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT =
  Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const commonStyles = StyleSheet.create({
  ///Common Styles for TEXT components
  title: {
    fontSize: options.xxlargeSize,
    color: options.brandTitle,
    fontFamily: options.fontFamilyBold,
  },
  /// Common Styles for NAVBAR
  navbar: {
    paddingBottom: 20,
    paddingTop: STATUS_BAR_HEIGHT + 20,
    paddingHorizontal: options.mediumOffset,
    alignSelf: 'stretch',
    width: width,
    backgroundColor: 'transparent',
    position: 'relative',
    zIndex: 5,
  },
  /// Common Styles for AVATAR
  avatar: {
    resizeMode: 'cover',
    position: 'relative',
  },
  avatarSelect: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderColor: '#959CA7',
    borderWidth: 1,
    position: 'absolute',
    top: 6,
    right: 6,
    backgroundColor: '#fff',
  },

  /// Common Styles for BOX
  boxStyle: {
    backgroundColor: options.brandWhite,
    elevation: 10,
    shadowColor: '#BEC2CE',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    borderRadius: options.borderRadiusBase,
  },

  ///Common Styles for HorizontalScroll

  horizontalScrollList: {
    height: 55,
    alignItems: 'center',
    flexDirection: 'row',
    elevation: 2,
    shadowColor: '#ACB1C0',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    backgroundColor: '#fff',
  },
  gridThree: {
    paddingHorizontal: 5,
    marginVertical: 5,
  },
  gridThreeNoOffset: {
    paddingHorizontal: 3,
    marginVertical: 3,
  },

  ///BOTTOM BAR STYLE

  Bottombar: {
    // width:width,
    // position:'absolute',
    // zIndex:10,
    // bottom:0,
    // left:0,
    shadowColor: options.blue,
    shadowOffset: { width: 0, height: -1 },
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 1,
    backgroundColor: options.white,
    height: 55,
    borderTopColor: options.blue,
    // borderTopLeftRadius: 15,
    // borderTopRightRadius: 15,
    zIndex: 1,
  },
  tabActionBtnHolder: {
    width: 76,
    elevation: 10,
    position: 'absolute',
    zIndex: 2,
    elevation: 2,
    bottom: 0,
    right: width / 2 - 38,
  },
  tabActionBtn: {
    position: 'absolute',
    zIndex: 2,
    left: 10,
    bottom: 26,
    opacity: 1,
    transform: [
      {
        rotate: '0deg',
      },
    ],
  },
  tabHolder: {
    backgroundColor: 'transparent',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    height: 55,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
  },

  fabHolder: {
    zIndex: 3,
    right: 0,
    bottom: 90,
    opacity: 1,

    position: 'absolute',
    width: 76,
    height: 200,
  },
  curveHolder: {
    backgroundColor: 'transparent',
    overflow: 'hidden',
    marginTop: 0,
    position: 'relative',
    width: 76,
    height: 55,
  },
  curveBkg: {
    width: 76,
    height: 76,
    borderRadius: 38,
    backgroundColor: options.brandlightGray,
    position: 'relative',
    marginTop: -40,
  },
  overlay: {
    position: 'absolute',
    width: 2 * width + 100,
    height: height,
    bottom: 27,
    left: -20,
    display: 'flex',
    opacity: 1,
    zIndex: 0,
  },

  overlayImg: {
    width: '100%',
    height: '100%',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    opacity: 1,
  },
  chatInput: {
    borderWidth: 1,
    borderColor: '#EAECEF',
    borderRadius: 60,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginRight: 20,
    fontSize: 15,
    fontFamily: options.fontFamily,
  },
  likeAction: {
    position: 'absolute',
    top: -5,
    left: 60,
    zIndex: 10,
    backgroundColor: '#fff',
    borderRadius: 12,
    width: 24,
    height: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    shadowColor: '#0A1F44',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },

  likeActionRight: {
    position: 'absolute',
    top: -5,
    right: 60,
    zIndex: 10,
    backgroundColor: '#fff',
    borderRadius: 12,
    width: 24,
    height: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    shadowColor: '#0A1F44',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },
  addCommentBox: {
    paddingHorizontal: options.mediumOffset,
    paddingTop: 10,
    paddingBottom: 30,
    backgroundColor: options.brandlightGray,
  },
  msgBadge: {
    borderRadius: 10,
    backgroundColor: options.brandPrimary,
    width: 19,
    height: 19,
    zIndex: 10,
    position: 'absolute',
    flexWrap: 'nowrap',
    right: -4,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff',
    top: -7,
  },

  msgBadgeText: {
    textTransform: 'uppercase',
    fontSize: 10,
    fontFamily: options.fontFamilyBold,
    color: '#fff',
    textAlign: 'center',
    width: '100%',
  },
});
export default commonStyles;
