import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import utility from '../../styles/Utitlity';
import { Icon } from 'native-base';

import Pic from '../../../images/google.svg';

const Demo = props => {
  return (
    <>
      <View
        style={[
          [
            utility.column,
            utility.mediumPaddingHorizontal,
            utility.justifyBetween,
            utility.blueBkg,
          ],
          { height: Dimensions.get('window').height },
        ]}
      >
        <View
          style={[
            utility.row,
            utility.greenBkg,
            utility.mediumMarginTop,
            utility.mediumPaddingVertical,
            utility.justifyBetween,
            utility.alignCenter,
            utility.xxradius,
          ]}
        >
          <View
            style={[
              utility.row,
              utility.alignCenter,
              utility.xsmallPaddingHorizontal,
            ]}
          >
            <Pic />
            <Text
              style={[
                utility.xsmallPaddingHorizontal,
                utility.whiteColor,
                utility.baseFontSize,
              ]}
            >
              Farid
            </Text>
          </View>
          <View style={[utility.row, utility.alignCenter]}>
            <View
              style={[
                utility.row,
                utility.alignCenter,
                utility.xsmallPaddingHorizontal,
              ]}
            >
              <Icon
                type="MaterialCommunityIcons"
                style={[utility.pinkColor]}
                name="medal"
              />
              <Text
                style={[
                  utility.xsmallPaddingHorizontal,
                  utility.whiteColor,
                ]}
              >
                1
              </Text>
            </View>
            <View
              style={[
                utility.row,
                utility.alignCenter,
                utility.xsmallPaddingHorizontal,
              ]}
            >
              <Icon
                type="SimpleLineIcons"
                style={[utility.blueColor]}
                name="diamond"
              />
              <Text
                style={[
                  utility.xsmallPaddingHorizontal,
                  utility.whiteColor,
                ]}
              >
                1
              </Text>
            </View>
            <View style={[utility.xsmallPaddingHorizontal]}>
              <Icon
                type="MaterialCommunityIcons"
                style={[utility.yellowColor]}
                name="treasure-chest"
              />
            </View>
          </View>
        </View>

        <View style={[utility.column]}>
          <View
            style={[
              utility.row,
              utility.justifyCenter,
              utility.alignCenter,
              utility.largeMarginBottom,
            ]}
          >
            <TouchableOpacity
              style={[
                [
                  utility.greenBkg,
                  utility.alignCenter,
                  utility.xxradius,
                  utility.mediumPaddingVertical,
                ],
                { flex: 1 },
              ]}
            >
              <Text
                style={[
                  utility.whiteColor,
                  utility.baseFontSize,
                ]}
              >
                شروع بازی
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={[
              utility.row,
              utility.justifyBetween,
              utility.alignCenter,
              utility.smallMarginBottom,
              utility.mediumPaddingVertical,
              utility.mediumPaddingHorizontal,
            ]}
          >
            <TouchableOpacity>
              <Icon
                type="FontAwesome"
                style={[utility.whiteColor]}
                name="cog"
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon
                type="Entypo"
                style={[utility.whiteColor]}
                name="lock"
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon
                type="MaterialCommunityIcons"
                style={[utility.whiteColor]}
                name="alert-box-outline"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};
export default Demo;
