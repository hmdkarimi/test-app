import React from 'react';
import { StatusBar, Platform } from 'react-native';
import * as Font from 'expo-font';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Router, Stack } from 'react-native-router-flux';
import { PersistGate } from 'redux-persist/es/integration/react';

import Routes from './routes/index';

// Hide StatusBar on Android as it overlaps tabs
if (Platform.OS === 'android') StatusBar.setHidden(true);

export default class App extends React.Component {
  static propTypes = {
    store: PropTypes.shape({}).isRequired,
    persistor: PropTypes.shape({}).isRequired,
  };

  state = { loading: true };

  async componentWillMount() {
    await Font.loadAsync({
      Bold: require('../fonts/SegoeUIBold.ttf'),
      BoldItalic: require('../fonts/SegoeUIBoldItalic.ttf'),
      Regular: require('../fonts/SegoeUI.ttf'),
      RegularItalic: require('../fonts/SegoeUIItalic.ttf'),
      Ionicons: require('../fonts/Ionicons.ttf'),
    });

    this.setState({ loading: false });
  }

  render() {
    const { loading } = this.state;
    const { store, persistor } = this.props;

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router>
            <Stack key="root">{Routes}</Stack>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}
