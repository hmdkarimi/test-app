import {
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const platformStyle = undefined;
const isIphoneX =
  platform === 'ios' &&
  deviceHeight === 812 &&
  deviceWidth === 375;

export default {
  platformStyle,
  platform,
  deviceHeight: deviceHeight,
  deviceWidth: deviceWidth,
  // Color
  dark: '#000000',
  blue: '#434FD9',
  gray: '#B0B1C5',
  pink: '#FF6464',
  green: '#4AC49E',
  yellow: '#FFE32E',
  white: '#FFFFFF',
  // brandTitle: "#0A1F44",
  // brandText: "#B4BBD1",
  // brandWhite: "#FFFFFF",
  // brandPrimary: "#FF2D55",
  // brandLike: "#BF5AF2",
  // brandBlue:'#0070FF',
  // brandGradientStart:"#FF3F64",
  // brandGradientEnd:"#FF77B1",
  // brandAlert: "#FF2D55",
  // brandSuccess: "#30D158",
  // brandInput: "#F1F2F6",
  // brandlightGray: '#F7F8FA',

  // Font
  fontFamilyNormal: 'Regular',
  fontFamilyNormalItalic: 'RegularItalic',
  fontFamilyBold: 'Bold',
  fontFamilyBoldItalic: 'BoldItalic',

  //Text
  baseSize: 17,
  regularSize: 15,
  smallSize: 14,
  largeSize: 22,
  xlargeSizex: 30,
  xlargeSize: 24,
  xxlargeSize: 34,

  // Title
  titleFontfamily: 'Bold',

  // Other
  borderRadiusBase: 6,
  xborderRadiusBase: 8,
  borderRadiusBox: 16,
  xborderRadiusBox: 500,

  ///OFFSETS

  baseOffset: 36,
  xbaseOffset: 18,
  mediumOffset: 20,
  xmediumOffset: 30,
  largeOffset: 40,
  smallOffset: 5,
  xsmallOffset: 10,
};
