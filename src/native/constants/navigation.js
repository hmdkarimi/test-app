import Colors from '../vars/variable';
import CommonStyles from '../styles/Common';
export default {
  navbarProps: {
    navigationBarStyle: { backgroundColor: 'trasnparent' },
    titleStyle: {
      color: Colors.brandText,
      alignSelf: 'center',
      letterSpacing: 2,
      fontSize: Colors.fontSizeBase,
    },
    backButtonTintColor: Colors.textColor,
  },

  tabProps: {
    swipeEnabled: true,
    activeBackgroundColor: 'transparent',
    inactiveBackgroundColor: Colors.blue,
    tabBarStyle: [CommonStyles.Bottombar],
    indicatorStyle: {
      backgroundColor: 'blue',
      top: 0,
      left: 0,
      position: 'relative',
      width: 5,
      height: 5,
    },
  },

  icons: {
    style: { color: 'white', height: 30, width: 30 },
  },
};
