import { errorMessages } from '../constants/messages';
import axios from 'axios';
axios.defaults.baseURL = 'http://165.22.234.156:8088';

export default {
  state: {}, // initial state

  /**
   * Reducers
   */
  reducers: {
    loading(state, loader) {
        return {
          ...state,
          loading: loader
        };
      },

      error(state, error) {
        return {
          ...state,
          error: error
        };
      },

      success(state, success) {
        return {
          ...state,
          success: success
        };
      },
  },

  /**
   * Effects/Actions
   */
  effects: dispatch => ({
    /**
     * Sign Up
     *
     * @param {obj} formData - data from form
     * @return {Promise}
     */
    // RECIPE // POST
    uploadImg(data) {

      this.loading(true);
      return new Promise(async (resolve, reject) => {
        // if (!name) return reject({ message: errorMessages.missingEmail });
        debugger
        axios({
            method: 'post',
            url: '/upload',
            responseType: 'json',
            data: data
          })
          .then(response => {
            debugger

            if (!response) {
               reject({
                message: response
              });
            } else {
              this.success('Image Uploaded');
              resolve(response.data);
            }
          })
          .catch(function (error) {
            debugger

             reject(error);
          });
      }).catch(err => {


        //throw err.message;
         reject();
      });
    },

    downloadImg(data) {

      this.loading(true);
      return new Promise(async (resolve, reject) => {
        // if (!name) return reject({ message: errorMessages.missingEmail });
        debugger
        axios({
            method: 'post',
            url: '/upload',
            responseType: 'json',
            data: data
          })
          .then(response => {
            debugger

            if (!response) {
               reject({
                message: response
              });
            } else {
              this.success('Image Uploaded');
            }
            return resolve();
          })
          .catch(function (error) {
            debugger

            reject();
          });
      }).catch(err => {

         reject({
          message: err
        });

      });
    },


  }),
};
