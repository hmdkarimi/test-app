import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';

class Demo extends React.Component {
  static propTypes = {
    Layout: propTypes.func.isRequired,
  };

  state = {};

  render = () => {
    const { Layout } = this.props;
    return <Layout />;
  };
}

const mapStateToProps = state => ({
  member: state.member || {},
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Demo);
